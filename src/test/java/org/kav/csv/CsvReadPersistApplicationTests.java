package org.kav.csv;

import org.junit.jupiter.api.Test;
import org.kav.csv.controller.model.RequestModel;
import org.kav.csv.service.CsvProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CsvReadPersistApplicationTests {
	@Autowired
	private CsvProcessorService csvProcessorService;

	@Test
	void readFile() {
		RequestModel requestModel = new RequestModel();
		requestModel.setInputFile("P:\\work\\6. trovicor - 05 08 2019\\convoy-data\\c6-100 areas\\20230528165330180_mfi_cdr-100-area.csv");
		requestModel.setDelimiter(";");
		csvProcessorService.process(requestModel);
	}

}
