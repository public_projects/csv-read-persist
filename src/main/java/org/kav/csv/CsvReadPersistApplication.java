package org.kav.csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsvReadPersistApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvReadPersistApplication.class, args);
	}

}
