package org.kav.csv.dao;

import org.kav.csv.dao.exception.DAOException;
import java.util.List;

public interface CsvRecordDAO {
    String IDENTITY_COLUMN_NAME = "identity";
    String TIMESTAMP_COLUMN_NAME = "timestampEpoch";
    String LONGITUDE_COLUMN_NAME = "longitude";
    String LATITUDE_COLUMN_NAME = "latitude";
    void createTable() throws DAOException;
    void batchPersist(List<Object[]> dataList);
    void fetchRange(long from, long to);
    void deleteTable();
    String getTableName();
}
