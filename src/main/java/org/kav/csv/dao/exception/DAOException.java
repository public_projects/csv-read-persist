package org.kav.csv.dao.exception;

public class DAOException extends Exception {
    public DAOException(String message, Exception exception){
        super(message, exception);
    }
}
