package org.kav.csv.dao.impl;

import org.kav.csv.dao.CsvRecordDAO;
import org.kav.csv.dao.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.List;

public class CsvRecordDAOImpl implements CsvRecordDAO {
    Logger logger = LoggerFactory.getLogger(CsvRecordDAOImpl.class);

    private JdbcTemplate jdbcTemplate;

    private final String createTableSyntax;

    private final String dropTableSyntax;

    private final String tableName;

    public CsvRecordDAOImpl(JdbcTemplate jdbcTemplate, String tableName) throws DAOException {
        this.jdbcTemplate = jdbcTemplate;
        this.tableName = "t" + tableName;
        this.createTableSyntax = "create table " + this.tableName + " (" +
                                 "id bigint auto_increment NOT NULL,\n"
                                 + IDENTITY_COLUMN_NAME + " varchar(255) NOT NULL,\n"
                                 + TIMESTAMP_COLUMN_NAME + " bigint NOT NULL,\n"
                                 + LONGITUDE_COLUMN_NAME + " double NOT NULL,\n"
                                 + LATITUDE_COLUMN_NAME + " double NOT NULL\n"
                                 + ");";
        this.dropTableSyntax = "drop table " + this.tableName + ";";
    }

    public void createTable() throws DAOException {
        logger.info("table: {} being created ....", tableName);

        try {
            jdbcTemplate.execute(createTableSyntax);
        } catch (BadSqlGrammarException e) {
            throw new DAOException("Table already exist: " + tableName, e);
        }
    }

    @Override
    public void batchPersist(List<Object[]> dataList) {
        String insertQuery =
                "INSERT INTO " + tableName + "("
                + TIMESTAMP_COLUMN_NAME + ", "
                + IDENTITY_COLUMN_NAME + ", "
                + LATITUDE_COLUMN_NAME + ", "
                + LONGITUDE_COLUMN_NAME + ")"
                + " VALUES" + "(?, ?, ?, ?)";
        jdbcTemplate.batchUpdate(insertQuery, dataList);
    }

    @Override
    public void fetchRange(long from, long to) {
    }

    @Override
    public void deleteTable() {
        this.jdbcTemplate.execute(dropTableSyntax);
    }

    @Override
    public String getTableName() {
        return this.tableName;
    }
}
