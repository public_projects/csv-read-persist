package org.kav.csv.dao.model;

public class CsvRecord {
    private final String identity;
    private final long timestamp;
    private final double longitude;
    private final double latitude;

    public CsvRecord(String identity, long timestamp, double longitude, double latitude) {
        this.identity = identity;
        this.timestamp = timestamp;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getIdentity() {
        return identity;
    }



    public long getTimestamp() {
        return timestamp;
    }



    public double getLongitude() {
        return longitude;
    }



    public double getLatitude() {
        return latitude;
    }




}
