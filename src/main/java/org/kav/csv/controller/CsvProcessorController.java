package org.kav.csv.controller;

import org.kav.csv.controller.model.RequestModel;
import org.kav.csv.controller.model.ResponseModel;
import org.kav.csv.service.CsvProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CsvProcessorController {

    @Autowired
    private CsvProcessorService csvProcessorService;

    @PostMapping(path = "/process/csv", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> processCsv(@RequestBody RequestModel requestBody) {
        return ResponseEntity.ok(csvProcessorService.process(requestBody));
    }
}
