package org.kav.csv.controller.model;

import org.kav.csv.config.CsvProcessStatus;

public class ResponseModel {
    private final String status;
    private final String message;

    public ResponseModel(CsvProcessStatus status, String message) {
        this.status = status.name();
        this.message = message;
    }

    public ResponseModel(String message) {
        this(CsvProcessStatus.FAIL, message);
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
