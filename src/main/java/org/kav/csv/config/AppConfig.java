package org.kav.csv.config;

import org.kav.csv.dao.CsvRecordDAO;
import org.kav.csv.dao.exception.DAOException;
import org.kav.csv.dao.impl.CsvRecordDAOImpl;
import org.kav.csv.service.CsvAsyncProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import java.time.format.DateTimeFormatter;

@Configuration
@EnableAsync
public class AppConfig {
    public static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Bean("csvRecordDAO")
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CsvRecordDAO getCsvRecordDAO(String tableName) throws DAOException {
        return new CsvRecordDAOImpl(jdbcTemplate, tableName);
    }

    @Bean("csvProcessor")
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CsvAsyncProcessor getCsvProcessor(CsvRecordDAO csvRecordDAO) {
        return new CsvAsyncProcessor(csvRecordDAO);
    }
}
