package org.kav.csv.config;

public enum CsvProcessStatus {
    SUCCESS, FAIL, IN_PROGRESS
}
