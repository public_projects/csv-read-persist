package org.kav.csv.service.exception;

public class CsvProcessorException extends Exception {

    public CsvProcessorException(String message) {
        super(message);
    }

    public CsvProcessorException(String message, Exception exception){
        super(message, exception);
    }
}
