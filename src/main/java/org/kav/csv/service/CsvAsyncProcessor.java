package org.kav.csv.service;

import org.kav.csv.config.AppConfig;
import org.kav.csv.dao.CsvRecordDAO;
import org.kav.csv.dao.impl.CsvRecordDAOImpl;
import org.kav.csv.dao.model.CsvRecord;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.kav.csv.service.exception.CsvProcessorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvAsyncProcessor {
    Logger logger = LoggerFactory.getLogger(CsvRecordDAOImpl.class);

    public static final String TIME_COLUMN_NAME = "time";

    private final CsvRecordDAO csvRecordDAO;

    public CsvAsyncProcessor(CsvRecordDAO csvRecordDAO){
        this.csvRecordDAO = csvRecordDAO;
    }

    @Async
    public List<CsvRecord> process(File inputFile, String delimiter) throws CsvProcessorException {
        logger.info("Processing {} ...", inputFile);
        long startTime = System.currentTimeMillis();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(inputFile))
                                        .withCSVParser(new CSVParserBuilder().withSeparator(delimiter.charAt(0)).build()).build()) {
            Map<String, Integer> columnIndex = getColumnIndexMapper(reader.readNext());
            String[] currLineArry;
            int batch = 500;
            List<Object[]> data = new ArrayList<>();
            for (int i = 0; (currLineArry =  reader.readNext()) != null; i++) {
                data.add(extracted(columnIndex, currLineArry));
                if (i == batch) {
                    csvRecordDAO.batchPersist(data);
                    i = 0;
                    data = new ArrayList<>();
                }
            }

            csvRecordDAO.batchPersist(data);
            logger.info("Records loaded into database in {} ms", System.currentTimeMillis() - startTime);
        } catch (Exception ex) {
            throw new CsvProcessorException("Failed to process: "+ inputFile.getName(), ex);
        }
        logger.info("Completed in {} ms.", System.currentTimeMillis() - startTime);
        return null;
    }

    private Map<String, Integer> getColumnIndexMapper(String[] header) throws CsvProcessorException {
        Map<String, Integer> columnIndexMap = new HashMap<>(4);
        if (header.length != 4) {
            throw new CsvProcessorException("Unexpected header! " + header);
        }

        for (int i = 0; i < header.length; i++) {
            String headerName = header[i].toLowerCase();
            if (headerName.equalsIgnoreCase(CsvRecordDAO.IDENTITY_COLUMN_NAME) ||
                headerName.equalsIgnoreCase(CsvRecordDAO.LATITUDE_COLUMN_NAME) ||
                headerName.equalsIgnoreCase(CsvRecordDAO.LONGITUDE_COLUMN_NAME)) {
                columnIndexMap.put(headerName.toLowerCase(), i);
            } else if (headerName.equalsIgnoreCase(TIME_COLUMN_NAME)) {
                columnIndexMap.put(CsvRecordDAO.TIMESTAMP_COLUMN_NAME, i);
            } else {
                throw new CsvProcessorException("Unsupported header! " + headerName);
            }
        }

        return columnIndexMap;
    }

    private Object[] extracted(Map<String, Integer> indexMapper, String[] data) {
        Object[] objArray = new Object[4];
        objArray[0] = LocalDateTime.parse(data[indexMapper.get(CsvRecordDAO.TIMESTAMP_COLUMN_NAME)],
                                          AppConfig.dateFormatter).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        objArray[1] = data[indexMapper.get(CsvRecordDAO.IDENTITY_COLUMN_NAME)];
        objArray[2] = Double.parseDouble(data[indexMapper.get(CsvRecordDAO.LATITUDE_COLUMN_NAME)]);
        objArray[3] = Double.parseDouble(data[indexMapper.get(CsvRecordDAO.LONGITUDE_COLUMN_NAME)]);
        return objArray;
    }
}
