package org.kav.csv.service;

import org.kav.csv.config.CsvProcessStatus;
import org.kav.csv.controller.model.RequestModel;
import org.kav.csv.controller.model.ResponseModel;
import org.kav.csv.dao.CsvRecordDAO;
import org.kav.csv.dao.exception.DAOException;
import org.kav.csv.dao.impl.CsvRecordDAOImpl;
import org.kav.csv.service.exception.CsvProcessorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileNotFoundException;

@Service
public class CsvProcessorService {
    Logger logger = LoggerFactory.getLogger(CsvRecordDAOImpl.class);

    @Autowired
    private BeanFactory beanFactory;

    public ResponseModel process(RequestModel requestModel) {
        File inputFile = null;
        CsvRecordDAO recordDAO = null;
        try {
            inputFile = getFile(requestModel.getInputFile());
            recordDAO = beanFactory.getBean(CsvRecordDAO.class, createTableName(inputFile.getName()));
            recordDAO.createTable();
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
            return new ResponseModel("Failed to process file: " + requestModel.getInputFile());
        } catch (DAOException e) {
            logger.error(e.getMessage(), e);
            return new ResponseModel(e.getMessage());
        }

        CsvAsyncProcessor asyncProcessing = beanFactory.getBean(CsvAsyncProcessor.class, recordDAO);

        try {
            asyncProcessing.process(inputFile, requestModel.getDelimiter());
        } catch (CsvProcessorException e) {
            logger.error("Failed to process: " + inputFile.getAbsolutePath(), e);
            return new ResponseModel(e.getMessage());
        }

        return new ResponseModel(CsvProcessStatus.IN_PROGRESS, "File: "+ requestModel.getInputFile() + ". Status: " + CsvProcessStatus.IN_PROGRESS.name());
    }

    private static String createTableName(String fileName){
        return fileName.toLowerCase().replaceAll("-", "_").replace(".csv","");
    }

    public static File getFile(String inputFilePath) throws FileNotFoundException {
        File f = new File(inputFilePath);
        if (!f.exists() || f.isDirectory()) {
            throw new FileNotFoundException("This is not a valid file path! : " + inputFilePath);
        }
        return new File(inputFilePath);
    }
}
